package traducteur.business;

import javax.ejb.Remote;

@Remote
public interface ITraducteurRemote {
	public String traduire(String fr);
	
	public void ajouterMot(String fr, String ang);
}
