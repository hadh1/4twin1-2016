package traducteur.business;

import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
//@LocalBean
public class Dictionnaire {

	private HashMap<String, String> mots;

	@PostConstruct
	public void remplirDictionnaire(){
		System.out.println("je suis instanciť");
		mots = new HashMap<>();
		mots.put("bonjour", "hello");
		mots.put("fille", "girl");
	}
	
	public HashMap<String, String> getMots() {
		return mots;
	}

	public void setMots(HashMap<String, String> mots) {
		this.mots = mots;
	}
	
	
}
