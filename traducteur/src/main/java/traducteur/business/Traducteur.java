package traducteur.business;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class Traducteur implements ITraducteurRemote{

	@EJB
	private Dictionnaire dictionnaire;
	
	public String traduire(String fr){
		return dictionnaire.getMots().get(fr);
	}
	
	public void ajouterMot(String fr, String ang){
		dictionnaire.getMots().put(fr, ang);
	}
	
}
