package hello.sessionbean;

import javax.ejb.Singleton;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

@Stateless
public class Hello implements IHelloRemote{
	
	public String sayHello(){
		return "hello";
		
	}
	
	public int returnHashcode(){
		return this.hashCode();
	}

}
